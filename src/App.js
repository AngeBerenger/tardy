import data from "./db/data";
import { Route, Routes } from "react-router-dom";
import Home from "./Pages/Home";
import Charts from "./Pages/Charts";
import NavBar from "./Components/NavBar";
import { Box, Stack } from "@mui/material";
import SlideBar from "./Components/SlideBar";
const { devices } = data;
const { mars } = data;
const { clients } = data;
function App() {
  const newLocal = (
    <Routes>
      <Route path="/" element={<Home  devices={devices} />} />
      <Route path="/charts" element={<Charts mars={mars} clients={clients} />} />
    </Routes>
  );

  return (
    <Box>
      <NavBar />
      <Stack direction="row" spacing={3} justifyContent="space-evenly">
      <SlideBar />
      {newLocal}
      </Stack> 
    </Box>
  );
}

export default App;
