import React from "react";

const GoMap = () => {
  return (
    <div>
      <iframe
        title="Google Maps"
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d44555.85904876859!2d4.718783312891223!3d45.736284891561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f4ebfab481e01d%3A0x408ab2ae4bb1d00!2s69110%20Sainte-Foy-l%C3%A8s-Lyon!5e0!3m2!1sfr!2sfr!4v1704820810363!5m2!1sfr!2sfr"
        width="99%"
        height="450"
        style={{ border: "0" }}
        allowFullScreen=""
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      ></iframe>
    </div>
  );
};

export default GoMap;
