import {
  AppBar,
  Avatar,
  Box,
  Toolbar,
  Typography,
  styled,
} from "@mui/material";
import React from "react";

export default function NavBar() {
  const StyledToolbar = styled(Toolbar)({
    display: "flex",
    justifyContent: "space-between",
    flexGrow: 1
  });

  const Icons = styled(Box)(({ theme }) => ({
    display: "none",
    alignItems: "center",
    gap: "20px",
    [theme.breakpoints.up("sm")]: {
      display: "flex",
    },
  }));

  return (
    <AppBar position="sticky">
      <StyledToolbar>
        <Typography variant="h6"  align="center" flexGrow={1}>DISP</Typography>
        <Icons>
          <Avatar alt="" src="" />
        </Icons>
      </StyledToolbar>
    </AppBar>
  );
}
