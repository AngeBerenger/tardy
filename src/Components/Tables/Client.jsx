import {
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@mui/material";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import AirportShuttleIcon from "@mui/icons-material/AirportShuttle";
import React from "react";

const Client = (props) => {
  const { clients } = props;
  return (
    <>
      <TableContainer component={Paper} sx={{ maxHeight: "250px" }}>
        <Table sx={{ minWidth: 500 }} aria-label="simple table">
          <TableBody>
            {clients.map((client) => (
              <TableRow key={client.id}>
                <TableCell sx={{ minWidth: 350 }}>{client.name}</TableCell>
                <TableCell align="left">
                  <Stack direction={"row"} spacing={3}>
                    {client.valueA} &nbsp;
                    <ZoomInIcon />
                    <AirportShuttleIcon />
                  </Stack>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default Client;
