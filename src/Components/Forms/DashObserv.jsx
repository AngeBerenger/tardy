import { Box, Button, Stack, TextField, Typography } from "@mui/material";
import DoneIcon from "@mui/icons-material/Done";
import CloseIcon from "@mui/icons-material/Close";
import React from "react";

function DashObserv() {
  return (
    <Stack spacing={6}>
      <Stack direction={"row"} spacing={12}>
        <TextField label="Observation" />
        <Button
          variant="contained"
          size="small"
          onClick={() => {
            alert("Envoyé");
          }}
        >
          SUBMIT
        </Button>
      </Stack>
      <Box>
        <Stack direction="row" spacing={2} justifyContent="space-evenly">
          <Typography variant="p">Evaluer Opération: </Typography>
          <Stack direction="row" spacing={2}>
            <Button variant="contained" size="meduim" color="error">
              <CloseIcon />
            </Button>

            <Button variant="contained" size="small" color="success">
              <DoneIcon />
            </Button>
          </Stack>
        </Stack>
      </Box>
    </Stack>
  );
}

export default DashObserv;
