import { Button, Stack, Typography } from "@mui/material";
import React from "react";
import DoneIcon from "@mui/icons-material/Done";
import CloseIcon from "@mui/icons-material/Close";

const Output = () => {
  return (
    <Stack>
      <Stack
        direction="row"
        spacing={2}
        justifyContent="space-evenly"
        sx={{
          marginBottom: "30px",
        }}
      >
        <Typography variant="p">Etat Outils: </Typography>
        <Stack direction="row" spacing={2}>
          <Typography variant="p">RAS </Typography>
        </Stack>
      </Stack>

      <Stack direction="row" spacing={2} justifyContent="space-evenly">
        <Typography variant="p">Evaluer Sortie: </Typography>
        <Stack direction="row" spacing={2}>
          <Button variant="contained" size="meduim" color="error">
            <CloseIcon />
          </Button>

          <Button variant="contained" size="small" color="success">
            <DoneIcon />
          </Button>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default Output;
