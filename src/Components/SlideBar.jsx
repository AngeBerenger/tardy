import { Box, List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material'
import React from 'react'
import DashboardIcon from '@mui/icons-material/Dashboard';
import PieChartIcon from '@mui/icons-material/PieChart';
import { NavLink } from 'react-router-dom';

export default function SlideBar() {
  return (
    <Box flex={1} sx={{ display: { xs: "none", sm: "block" } }}>
      {/* <h4>Bienvenue</h4> */}
      <List>
        <ListItem disablePadding>
          <NavLink
            to="/"
            className="nav-link p-0"
            style={{ textDecoration: 'none', color: 'inherit' }}
          >
            <ListItemButton>
              <ListItemIcon>
                <DashboardIcon />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItemButton>
          </NavLink>
        </ListItem>

        <ListItem disablePadding >
          <NavLink
            to="/charts"
            className="nav-link p-0"
            style={{ textDecoration: 'none', color: 'inherit' }}
          >
            <ListItemButton>
              <ListItemIcon>
                <PieChartIcon />
              </ListItemIcon>
              <ListItemText primary="Chart" />
            </ListItemButton>
          </NavLink>
        </ListItem> 
      </List>
    </Box>
  )
}
