import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  ReferenceLine,
} from "recharts";

const data = [
  {
    name: "10:07:15",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "10:07:18",
    uv: -3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "10:07:21",
    uv: -2000,
    pv: -9800,
    amt: 2290,
  },
  {
    name: "10:07:24",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "10:07:27",
    uv: -1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "10:07:30",
    uv: 2390,
    pv: -3800,
    amt: 2500,
  },
];

export default function TimesSeries() {
  return (
    <>
      <BarChart
        width={550}
        height={300}
        data={data}
        stackOffset="sign"
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <ReferenceLine y={0} stroke="red" />
        <Bar dataKey="pv" fill="#8884d8" stackId="stack" />
        <Bar dataKey="uv" fill="#82ca9d" stackId="stack" />
      </BarChart>
    </>
  );
}
