import React from "react";
import Chart from "react-apexcharts";

const options = {
  plotOptions: {
    radialBar: {
      track: {
        background: "#f2f2f2",
        strokeWidth: "20%",
      },
      dataLabels: {
        show: true,
        value: {
          show: true,
          fontSize: "16px",
        },
        total: {
          show: true,
          label: "Sortie",
          color: "#373d3f",
        },
      },
    },
  },
};

export default function OutLevel() {
  return (
    <>
      <Chart options={options} series={[66]} type="radialBar" width="200" />
    </>
  );
}
