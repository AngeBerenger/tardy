import React from "react";
import Chart from "react-apexcharts";

const options = {
  plotOptions: {
    radialBar: {
      track: {
        background: "#f2f2f2",
        strokeWidth: "70%"
      },
      dataLabels: {
        show: true,
        value: {
          show: true,
          fontSize: "16px",
        },
        total: {
          show: true,
          label: "Propre",
          color: "#373d3f",
        },
      },
    },
  },
};

export default function Clean() {
  return (
      <> 
        <Chart options={options} series={[33]}  type="radialBar" width="200" />
      </> 
 
  );
}
