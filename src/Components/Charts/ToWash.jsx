import React from "react";
import Chart from "react-apexcharts";

const options = {
  plotOptions: {
    radialBar: {
      track: {
        background: "#f2f2f2",
        strokeWidth: "70%",
      },
      dataLabels: {
        show: true,
        value: {
          show: true,
          fontSize: "16px",
        },
        total: {
          show: true,
          label: "A laver",
          color: "#373d3f",
        },
      },
    },
  },
};

export default function ToWash() {
  return (
    <>
      <Chart options={options} series={[0]} type="radialBar" width="200" />
    </>
  );
}
