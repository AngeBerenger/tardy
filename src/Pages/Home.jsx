import React from "react";
import { Box, Button, Grid, Paper, Typography } from "@mui/material";
import TimesSeries from "../Components/Charts/TimesSeries";
import Device from "../Components/Tables/Device";
import DashObserv from "../Components/Forms/DashObserv";
import Output from "../Components/Forms/Output";
import PowerSettingsNewIcon from "@mui/icons-material/PowerSettingsNew";

const Home = (props) => {
  const { devices } = props;

  return (
    <Box flex={8}>
      <Box sx={{ mt: 4, mb: 4 }}>
        <Grid container spacing={2}>
          {/* Start --- Timesseries chart */}
          <Grid item xs={12} md={6} lg={6}>
            <Paper sx={{ backgroundColor: "rgba(169,169,169,0.1)" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  paddingTop: "10px",
                }}
              >
                <Typography variant="p">Timesseries chart</Typography>
              </div>
              <TimesSeries />
            </Paper>
          </Grid>
          {/* End --- Timesseries chart */}

          {/* Start --- Device table */}
          <Grid item xs={12} md={6} lg={6}>
            <Paper
              sx={{
                display: "flex",
                flexDirection: "column",
                height: 330,
              }}
            >
              <Device devices={devices} />
            </Paper>
          </Grid>
          {/* End --- Device table */}
        </Grid>

        <Grid
          container
          spacing={2}
          sx={{
            marginTop: "5px",
          }}
        >
          {/* Start --- Model out control */}
          <Grid item xs={12} md={6} lg={6}>
            <Paper
              sx={{
                p: 2,
                display: "flex",
                flexDirection: "column",
                height: 240,
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  paddingTop: "10px",
                  marginBottom: "30px",
                }}
              >
                <Typography variant="h6">Model Output</Typography>
              </div>

              <Output />
            </Paper>
          </Grid>
          {/* End --- Model out control */}

          {/* Start --- Observation form */}

          <Grid item xs={12} md={6} lg={6}>
            <Paper
              sx={{
                p: 2,
                display: "flex",
                flexDirection: "column",
                height: 240,
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  paddingTop: "10px",
                  marginBottom: "30px",
                }}
              >
                <Typography variant="h6">Form</Typography>
              </div>
              <DashObserv />
            </Paper>
          </Grid>
          {/* End --- Observation form */}
        </Grid>
        
        {/* Start --- Power control*/}
        <Grid
          container
          spacing={2}
          sx={{
            marginTop: "30px",
          }}
        >
          <Grid item xs={12} md={12} lg={12}>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                paddingTop: "10px",
                marginBottom: "30px",
              }}
            >
              <Button
                variant="contained"
                startIcon={<PowerSettingsNewIcon />}
                color="success"
                onClick={() => {
                  alert("Le système est lancé.");
                }}
              >
                LANCER
              </Button>
            </div>
          </Grid>
        </Grid>
        {/* End --- Power control*/}
      </Box>
    </Box>
  );
};

export default Home;
