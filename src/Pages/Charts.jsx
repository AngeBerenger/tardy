import { Box, Paper, Stack, Typography } from "@mui/material";
import React from "react";
import OutLevel from "../Components/Charts/OutLevel";
import ToWash from "../Components/Charts/ToWash";
import Clean from "../Components/Charts/Clean";
import Client from "../Components/Tables/Client";
import Mars from "../Components/Tables/Mars";
import GoMap from "../Components/Frames/GoMap";

const Charts = (props) => {
  const { clients, mars } = props;
  return (
    <Box flex={8}>
      <Box sx={{ mt: 4, mb: 4 }}>
        <Stack direction={"row"} spacing={2}>
          <Stack>
            {/* ETAT */}

            <Paper
              xs={10}
              sx={{
                backgroundColor: "rgba(169,169,169,0.1)",
                fontSize: "13px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  paddingTop: "10px",
                  marginLeft: "5px",
                }}
              >
                <Typography variant="p">ETAT DU STOCK</Typography>
              </div>
              <Stack direction={"row"} spacing={-6}>
                <OutLevel />
                <ToWash />
                <Clean />
              </Stack>
            </Paper>

            {/* MAP */}
            <Paper
              xs={10}
              sx={{
                backgroundColor: "rgba(169,169,169,0.1)",
                marginTop: "10px",
                fontSize: "13px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  paddingTop: "20px",
                  marginLeft: "50px",
                }}
              >
                <Typography variant="p">PROCHAINE TOURNEE</Typography>
                <Stack
                  direction="row"
                  spacing={2}
                  justifyContent="space-evenly"
                  sx={{
                    marginBottom: "20px",
                    marginTop: "30px",
                  }}
                >
                  <Typography variant="p">Date:13/04/23 </Typography>
                  <Typography variant="p">
                    Conducteur: Martin R.{" "}
                  </Typography>{" "}
                </Stack>
              </div>

              <GoMap />
            </Paper>
          </Stack>
          <Stack>
            {/* Client */}
            <Paper
              sx={{ display: { xs: "none", sm: "block" }, fontSize: "13px" }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  paddingTop: "10px",
                  marginLeft: "5px",
                  marginBottom: "10px",
                }}
              >
                <Typography variant="p">CLIENTS</Typography>
              </div>
              <Client clients={clients} />
            </Paper>
            {/* Mays */}
            <Paper
              sx={{
                height: 285,
                marginTop: "10px",
                display: { xs: "none", sm: "block" },
                fontSize: "13px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  paddingTop: "10px",
                  marginLeft: "5px",
                  marginTop: "15px",
                  marginBottom: "10px",
                }}
              >
                <Typography variant="p">STATISTQUE DE MARS</Typography>
              </div>
              <Mars mars={mars} />
            </Paper>
          </Stack>
        </Stack>
      </Box>
    </Box>
  );
};

export default Charts;
